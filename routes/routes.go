package routes

import (
	"a_gin_scaffold/logger"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"net/http"
)

func SetUp() *gin.Engine {
	r := gin.Default()
	// 读取配置是否使用统一的logger
	if viper.GetBool("app.logprint") {
		r = gin.New()
		r.Use(logger.GinLogger(), logger.GinRecovery(true))
	}
	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "ok")
	})
	r.GET("/xi", func(c *gin.Context) {
		c.String(http.StatusOK, "hi xiaosheng ")
	})
	return r
}
