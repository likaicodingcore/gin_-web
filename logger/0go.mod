module logger

go 1.17

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/spf13/viper v1.10.0
	go.uber.org/zap v1.19.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
