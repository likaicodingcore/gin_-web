package setting

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

func Init() (err error) {
	// 设置默认值
	viper.SetDefault("fileDir", "")
	viper.SetConfigName("config")         // 配置文件名称中没有扩展名
	viper.SetConfigType("yaml")           // 如果配置文件没有扩展名，这需要配置此项
	viper.AddConfigPath(".")              // 查找配置文件所在路径
	viper.AddConfigPath("$HOME/.appname") // 多次调用以添加多个搜索路径
	err = viper.ReadInConfig()            // 查找并读取配置文件
	if err != nil {
		//fmt.Println("main: viper initial failed:", err.Error())
		return err
		//panic(fmt.Errorf("main: config initial failed"))
	}
	viper.WatchConfig()
	viper.OnConfigChange(func(in fsnotify.Event) {
		fmt.Printf("[*]config changed:...:%v\n", in.Name)
	})
	return
}
